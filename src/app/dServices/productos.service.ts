import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class ProductosService 
{

  public productos = [];

  constructor( private http: HttpClient ) 
  { 

  }

  obtenerProductos( categoria: string = 'false', estado: string = 'true' )
  {

    return this.http.get(`${ base_url }/productos?categoria=${ categoria }&estado=${ estado }`)
      .pipe
      (
        map( (resp: any) => 
        {
          this.productos = resp.data;
          return resp.data;
        })
      );

  }



}
