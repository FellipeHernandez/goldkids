import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestHttpService } from 'src/app/_services/rest-http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-td-enviar-amigo',
  templateUrl: './td-enviar-amigo.component.html',
  styleUrls: ['./td-enviar-amigo.component.css']
})
export class TdEnviarAmigoComponent implements OnInit {
  enviarAmigo: FormGroup;
  submitted  = false;
  arreglo: any;
  allCountries: any;


  constructor(private formBuilder: FormBuilder,
    public restHttpService: RestHttpService,
    private route: ActivatedRoute,
    private router: Router,) { }

  ngOnInit() {
    window.scroll(0,0)
    this.enviarAmigo = this.formBuilder.group({
      nombres: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],

    })
  }
  scrollUp(){
    window.scroll(0,0);
  }

  enviar(){
  //Obtener los datos que proviene de los inputs--
      //Declarar los Inputs
      var nombres: any = this.enviarAmigo.get("nombres").value;
      var email: any = this.enviarAmigo.get("email").value;

      //Declaracion del arreglo que se envia
      this.arreglo = {
          nombres: nombres,
          email: email,

      }


      console.log("DATOS:: ", this.arreglo)
      console.log("Invalido: ", this.enviarAmigo.invalid);
      // if (this.enviarAmigo.invalid === false) {
      //     //---------------------  Suscripcion al servicio ------------------------------------>
      //     //#- Aca se definen los diferentes metodos para acceder a la API. 
      //     this.restHttpService.registerUser(this.arreglo).subscribe(
      //         data => {

      //             console.log("ITEM:", this.arreglo);
      //             console.log("DATO RECIBIDO: ", data);
      //             this.allCountries = Object.values(data);
      //             this.enviarAmigo.reset();

      //         },
      //         // Manejo de errores.
      //         error => {
      //             console.log("ocurrio el siguiente error");
      //             console.log(error.errorMessage);
      //         }
      //     );
      // } else {
      //     // this.toastr.error('¡Todos los campos son obligatorios!', 'Error de Registro');
      // }
      this.restHttpService.sendMessage(this.arreglo).subscribe(
          data => {

              /* console.log("ITEM:",this.arreglo1);
              this.allCountries = Object.values(data);*/
              console.log("Email Recibido: ", data);
              // this.toastr.success('¡EMAIL ENVIADO CORRECTAMENTE!', 'Registro Exitoso');
          },
          // Manejo de errores.
          error => {
              console.log("ocurrio el siguiente error");
              console.log(error.errorMessage);
          }
      );

  }



  get f() { return this.enviarAmigo.controls; }


  onSubmit() {
    this.submitted = true;

    if (this.enviarAmigo.invalid) {
        return;
    } 


  }




}
