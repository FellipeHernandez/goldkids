import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, ReactiveFormsModule, FormsModule, FormBuilder, Validators } from '@angular/forms';
import { RestHttpService } from '../../_services/rest-http.service';
import { HttpClient } from 'selenium-webdriver/http';
import { ActivatedRoute, Router } from '@angular/router';
import {TdCompraComponent} from '../td-compra/td-compra.component'
import { ProductosService } from '../../dServices/productos.service';
import { CategoriasService } from '../../dServices/categorias.service';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
    contactos: FormGroup;
    submitted = false;
    arreglo: any;
    allCountries: any;
    mostrar: Boolean = false;
    verConjuntos: Boolean = true;
    verVestidos: Boolean = false;
    verPijamas: Boolean = false;
    verVariedad: Boolean = false;
    tipo: any;
    mostrarTexto: boolean = false;
    cambio: boolean = true;
    argollas:any;
    product:any;
    ocultarForm: boolean = false
    allDates: any;
    titulo: any;
    allDatesPar: any;
    allDatesImpar: any;

    public productos = [];
    public categorias = [];

    listaConjuntos = 
    [
      // Conjuntos de niñas
      {
        referencia: 386,
        talla: "2,4,6,8,10",
        color: "azul claro,verde agua,rosado claro",
        titulo: "Conjunto niña-perchada estampado",
        imagen: '../../../assets/images/conjuntonias/2A.jpg',          
        precio: 24900,
      },
      {
        referencia: 372,
        talla: "1,2,4,6",
        color: "Unico",
        titulo: "Conjunto niña-Tela tejido plano",
        imagen: '../../../assets/images/conjuntonias/1A.jpg',            
        precio: 34000,
      },
      {
        referencia: 383,
        talla: "1, 2, 3, 4, 6",
        color: "Rojo, rosado, azul",
        titulo: "Conjunto de niña estampado-variedad",
        imagen: '../../../assets/images/conjuntonias/3A.jpg',            
        precio: 24900,
      },
      {
        referencia: 6,
        talla: "1,2,4,6",
        color: "Surtido",
        titulo: "Conjunto de niña estampado-variedad",
        imagen: '../../../assets/images/conjuntonias/4A.jpg',            
        precio: 12000,
      },
      {
        referencia: 384,
        talla: "0, 1, 2",
        color: "Rosado, fucsia, gris",
        titulo: "Conjunto de niña tela burda y licra algodón",
        imagen: '../../../assets/images/conjuntonias/5A.jpg',            
        precio: 31900,
      },
      {
        referencia: 385,
        talla: "1,2,4,6",
        color: "Rojo, gris, azul petróleo",
        titulo: "Conjunto de niña licra algodón burda",
        imagen: '../../../assets/images/conjuntonias/6A.jpg',            
        precio: 25900,
      },
      {
        referencia: 47,
        talla: "1,2,4,6",
        color: "Blanco, rosado, vinotinto, fucsia",
        titulo: "Conjunto de niña tela burda",
        imagen: '../../../assets/images/conjuntonias/7A.jpg',            
        precio: 34900,
      },
      {
        referencia: 47,
        talla: "1,2,4,6",
        color: "Blanco, rosado, vinotinto, fucsia",
        titulo: "Conjunto de niña tela burda estampado",
        imagen: '../../../assets/images/conjuntonias/8A.jpg',            
        precio: 34900,
      },
      // Conjuntos de niños
      {
        referencia: 390,
        talla: "0,1,2,4",
        color: "Gris, rojo, negro, azul oscuro",
        titulo: "Conjunto de niño tela burda y algodón",
        imagen: '../../../assets/images/conjuntosnios/1O.jpg',            
        precio: 34000,
      },
      {
        referencia: 24,
        talla: "0,1,2",
        color: "Gris, rojo, azul, blanco",
        titulo: "Conjunto de niño variedad de estampados",
        imagen: '../../../assets/images/conjuntosnios/2O.jpg',            
        precio: 18000,
      },
      {
        referencia: 24,
        talla: "0,1,2",
        color: "Gris, rojo, azul, blanco",
        titulo: "Conjunto de niño variedad de estampados",
        imagen: '../../../assets/images/conjuntosnios/3O.jpg',            
        precio: 18000,
      },
      {
        referencia: 30,
        talla: "0,1,2",
        color: "Rojo, gris, azul oscuro",
        titulo: "Conjunto de niño estampado",
        imagen: '../../../assets/images/conjuntosnios/4O.jpg',            
        precio: 27000,
      },
      {
        referencia: 389,
        talla: "1,2,4,6,8",
        color: "Rojo, azul, gris, negro",
        titulo: "Conjunto de niño estampado superman",
        imagen: '../../../assets/images/conjuntosnios/5O.jpg',            
        precio: 38000,
      },
      {
        referencia: 384,
        talla: "1,2,4,6,8",
        color: "Rojo, azul, gris, negro",
        titulo: "Conjunto de niño estampado spiderman",
        imagen: '../../../assets/images/conjuntosnios/6O.jpg',            
        precio: 38000,
      },
      {
        referencia: 387,
        talla: "0,1,2",
        color: "Beis, azul oscuro",
        titulo: "Conjunto de niño overol",
        imagen: '../../../assets/images/conjuntosnios/7O.jpg',            
        precio: 24900,
      },
      {
        referencia: 373,
        talla: "0,1,2",
        color: "Único",
        titulo: "Conjunto de niño tela tejido plano",
        imagen: '../../../assets/images/conjuntosnios/8O.jpg',            
        precio: 24000,
      },
      {
        referencia: 44,
        talla: "1,2,4",
        color: "Surtido",
        titulo: "Conjunto de niño estemapado variado",
        imagen: '../../../assets/images/conjuntosnios/9O.jpg',            
        precio: 22000,
      },
      {
        referencia: 44,
        talla: "1,2,4,6,8,10",
        color: "Surtido",
        titulo: "Conjunto de niño estemapado variado",
        imagen: '../../../assets/images/conjuntosnios/10O.jpg',            
        precio: 19900,
      },
    ]

    listaVestidos = 
    [
      {
        referencia: 388,
        talla: "0,1,2,4,6",
        color: "Rosado, rojo y azul oscuro",
        titulo: "Vestido enresortado forrado, balaca y calzones",
        imagen: '../../../assets/images/Vestidos/1.jpg',          
        precio: 21900,
      },
      {
        referencia: 375,
        talla: "0,1,2,4,6",
        color: "Rosado, rojo y azul",
        titulo: "Vestido tejido plano",
        imagen: '../../../assets/images/Vestidos/2.jpg',          
        precio: 34000,
      },
      {
        referencia: 43,
        talla: "1,2,4,6",
        color: "Rojo, negro, rosado",
        titulo: "Vestido plano licra",
        imagen: '../../../assets/images/Vestidos/3.jpg',          
        precio: 24900,
      },
    ]

    listaPijamas =
    [
      // Pijamas niñas
      {
        referencia: 380,
        talla: "0,1,2,4,6,8,10",
        color: "Surtidos",
        titulo: "Pijama niñas manga larga",
        imagen: '../../../assets/images/pijamanias/1.jpg',          
        precio: 18000,
      },
      {
        referencia: 380,
        talla: "0,1,2,4,6,8,10",
        color: "Surtidos",
        titulo: "Pijama niñas manga larga",
        imagen: '../../../assets/images/pijamanias/2.jpg',          
        precio: 18000,
      },
      {
        referencia: 380,
        talla: "0,1,2,4,6,8,10",
        color: "Surtidos",
        titulo: "Pijama niñas manga larga",
        imagen: '../../../assets/images/pijamanias/3.jpg',          
        precio: 18000,
      },
      {
        referencia: 380,
        talla: "0,1,2,4,6,8,10",
        color: "Surtidos",
        titulo: "Pijama niñas manga larga",
        imagen: '../../../assets/images/pijamanias/4.jpg',          
        precio: 18000,
      },
      {
        referencia: 0,
        talla: "1,2,4,6,8,10",
        color: "Rojo, rosado, azul",
        titulo: "Pijama niñas manga larga",
        imagen: '../../../assets/images/pijamanias/P1A.png',          
        precio: 25000,
      },
      // Pijamas niños
      {
        referencia: 381,
        talla: "0,1,2,4,6,8,9,10",
        color: "Rojo, azul, gris",
        titulo: "Pijama niños manga larga estampado surtido",
        imagen: '../../../assets/images/pijamanios/1.jpg',          
        precio: 18000,
      },
      {
        referencia: 15,
        talla: "1,2,4,6,8,10",
        color: "Surtidos",
        titulo: "Pijama niños manga corta estampado surtido",
        imagen: '../../../assets/images/pijamanios/2.jpg',          
        precio: 17000,
      },
      {
        referencia: 15,
        talla: "1,2,4,6,8,10",
        color: "Surtidos",
        titulo: "Pijama niños manga corta estampado surtido",
        imagen: '../../../assets/images/pijamanios/3.jpg',          
        precio: 17000,
      },
      {
        referencia: 15,
        talla: "1,2,4,6,8,9,10",
        color: "Surtidos",
        titulo: "Pijama niños manga larga estampado surtido",
        imagen: '../../../assets/images/pijamanios/4.jpg',          
        precio: 18000,
      },
      {
        referencia: 381,
        talla: "0,1,2,4,6,8,9,10",
        color: "Rojo, azul, gris",
        titulo: "Pijama niños manga larga estampado surtido",
        imagen: '../../../assets/images/pijamanios/5.jpg',          
        precio: 18000,
      },
      {
        referencia: 16,
        talla: "0,1,2,4,6,8,10",
        color: "Surtidos",
        titulo: "Pijama niños manga corta estampado surtido",
        imagen: '../../../assets/images/pijamanios/6.jpg',          
        precio: 18000,
      },
    ]

    listaVariedad = 
    [
      {
        referencia: 5,
        talla: "0,1,2",
        color: "Salmón y rosado",
        titulo: "Sudadera de niña",
        imagen: '../../../assets/images/Variedad/1.jpg',          
        precio: 10000,
      },
      {
        referencia: 7,
        talla: "1,2,4,6",
        color: "Gris",
        titulo: "Sudadera de niña estampada",
        imagen: '../../../assets/images/Variedad/2.jpg',          
        precio: 10000,
      },
      {
        referencia: 20,
        talla: "1,2,4",
        color: "Verde militar",
        titulo: "Pantalón niño",
        imagen: '../../../assets/images/Variedad/3.jpg',          
        precio: 22000,
      },
      {
        referencia: 39,
        talla: "1,2,4,6,8",
        color: "Azul, rojo, negro",
        titulo: "Pantalón de niño",
        imagen: '../../../assets/images/Variedad/4.jpg',          
        precio: 22000,
      },
      {
        referencia: 13,
        talla: "1,2,4,6,8",
        color: "Surtidos",
        titulo: "Camisas niños",
        imagen: '../../../assets/images/Variedad/5.jpg',          
        precio: 25000,
      },
    ]
  
    constructor(
      private productosService: ProductosService,
      private categoriasService: CategoriasService,
      private formBuilder: FormBuilder,
      public restHttpService: RestHttpService,
      private route: ActivatedRoute,
      private router: Router,
    ) { }
  
    ngOnInit() {

      // Daniel

      this.obtenerProductos();

      this.categoriasService.obtenerCategorias()
      .subscribe( resp => 
      {
        this.categorias = resp;
      }); 

      // Fin Daniel
      
      window.scroll(0,0)

      this.contactos = this.formBuilder.group({
        name: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        mensaje: ['', Validators.required],
  
      })
      this.argollas=[
          {
          "imagen":"argolla1.jpg"
          },
          {
            "imagen":"argolla2.jpg"
          },
          {
            "imagen":"argolla3.jpg"
          },
      ]
  
  
    }

    /* Métodos Daniel */

    obtenerProductos(categoria: string = 'false', estado: string = 'true')
    {
      this.productosService.obtenerProductos(categoria, estado)
        .subscribe( resp => 
        {
          this.productos = resp;
        }); 
    }




    /* Fin Métodos Daniel */



  
    scrollUp() {
      window.scroll(0, 0);
    }
  
    ver() {
      this.mostrar = true;
      
    }
  
    verTexto() {
      this.mostrarTexto = true;
      
    }
  
    verMenos() {
      this.mostrarTexto = false;
      
    }
  
  
    tipoM(tipo) {
      this.titulo = tipo;
      //  this.formProducto.get('tipoP').setValue(tipo);
       //this.formProducto.get('descripcion').setValue(tipo);
       console.log("TIPO::",tipo)
       this.submitted = false
       //this.formProducto.reset();
       switch (tipo) {
         case "conjuntos":
           this.ocultarForm = true;
           this.verConjuntos = true;
           this.verPijamas = false;
           this.verVestidos = false;
           this.verVariedad = false;
           
               this.product = {
                 tipoProducto: "conjuntos"
               }
             /*  this.restHttpService.getProduct(this.product).subscribe(
                 data => {
   
                     console.log("ITEM:", this.arreglo);
                     console.log("DATO RECIBIDO: ", data);
                     this.allDates = Object.values(data);
                    //  this.allDates = this.allDates.filter(function(dato){ 
                    //   if(dato.indexOf("") == 0)
                    //      { 
                    //        console.log("posicion::::::", dato.indexOf())
                    //        return false; 
                    //      }
                    //    else{  
                    //      return true;     
                        
                    //    } 
                    //  });
                     
                     console.log("::::::::::::::::::::::",this.allDates)
                     this.allDatesPar;
                     this.allDatesImpar;
                     this.submitted = false
                    
                   //  this.formProducto.reset();
   
                 },
                 // Manejo de errores.
                 error => {
                     console.log("ocurrio el siguiente error");
                     console.log(error.errorMessage);
                 }
               );*/

               
              

           break;
         case "pijamas":
             this.ocultarForm = true;
           this.verPijamas = true;
           this.verVestidos = false;
           this.verVariedad = false;
           this.verConjuntos = false;
           this.product = {
             tipoProducto: "pijamas"
           }
           /* this.restHttpService.getProduct(this.product).subscribe(
             data => {
   
                 console.log("ITEM:", this.arreglo);
                 console.log("DATO RECIBIDO: ", data);
                 this.allDates = Object.values(data);
                 this.submitted = false
                 // this.formProducto.reset();
               //  this.formProducto.reset();
   
             },
             // Manejo de errores.
             error => {
                 console.log("ocurrio el siguiente error");
                 console.log(error.errorMessage);
             }
           ); */
           break;
         case "vestidos":
           this.ocultarForm = true;
           this.verVestidos = true;
           this.verVariedad = false;
           this.verConjuntos = false;
           this.verPijamas = false;
           this.product = {
             tipoProducto: "vestidos"
           }
           /* this.restHttpService.getProduct(this.product).subscribe(
             data => {
   
                 console.log("ITEM:", this.arreglo);
                 console.log("DATO RECIBIDO: ", data);
                 this.allDates = Object.values(data);
                 this.submitted = false
                 
               //  this.formProducto.reset();
   
             },
             // Manejo de errores.
             error => {
                 console.log("ocurrio el siguiente error");
                 console.log(error.errorMessage);
             }
           ); */
           break;
         case "variedad":
           this.ocultarForm = true;
           this.verVariedad = true;
           this.verConjuntos = false;
           this.verPijamas = false;
           this.verVestidos = false;
           this.product = {
             tipoProducto: "variedad"
           }
           /* this.restHttpService.getProduct(this.product).subscribe(
             data => {
   
                 console.log("ITEM:", this.arreglo);
                 console.log("DATO RECIBIDO: ", data);
                 this.allDates = Object.values(data);
                 this.allDatesPar;
                 this.allDatesImpar;
                 /* for(let i = 0; this.allDates.length; i++){
                   if(i % 0){
                    this.allDatesPar = this.allDates[i]
                   }
                   if(i !% 0){
                    this.allDatesImpar = this.allDates[i]
                   }

                 } 
                 this.submitted = false
                 // this.formProducto.reset();
               //  this.formProducto.reset();
   
             },
             // Manejo de errores.
             error => {
                 console.log("ocurrio el siguiente error");
                 console.log(error.errorMessage);
             }
           ); */
           break;
        
            }
          }
  
  
    enviar() {
      //Obtener los datos que proviene de los inputs--
      //Declarar los Inputs
      var nombresJuridica: any = this.contactos.get("name").value;
      var email: any = this.contactos.get("email").value;
      var mensaje: any = this.contactos.get("mensaje").value;
  
      //Declaracion del arreglo que se envia
      this.arreglo = {
        nombres: name,
        email: email,
        mensaje: mensaje,
  
      }
  
  
      console.log("DATOS:: ", this.arreglo)
      console.log("Invalido: ", this.contactos.invalid);
      if (this.contactos.invalid === false) {
        //---------------------  Suscripcion al servicio ------------------------------------>
        //#- Aca se definen los diferentes metodos para acceder a la API. 
        this.restHttpService.createProduct(this.arreglo).subscribe(
          data => {
  
            console.log("ITEM:", this.arreglo);
            console.log("DATO RECIBIDO: ", data);
            this.allCountries = Object.values(data);
            this.contactos.reset();
            alert("Mensaje enviado correctamente")
  
          },
          // Manejo de errores.
          error => {
            console.log("ocurrio el siguiente error");
            console.log(error.errorMessage);
          }
        );
      } else {
        // this.toastr.error('¡Todos los campos son obligatorios!', 'Error de Registro');
      }
      this.restHttpService.sendMessage(this.arreglo).subscribe(
        data => {
  
          /* console.log("ITEM:",this.arreglo1);
          this.allCountries = Object.values(data);*/
          console.log("Email Recibido: ", data);
          // this.toastr.success('¡EMAIL ENVIADO CORRECTAMENTE!', 'Registro Exitoso');
        },
        // Manejo de errores.
        error => {
          console.log("ocurrio el siguiente error");
          console.log(error.errorMessage);
        }
      );
  
    }
  
  
  
    get f() { return this.contactos.controls; }
  
  
    onSubmit() {
      this.submitted = true;
  
      if (this.contactos.invalid) {
        return;
      }
  
    }

    clickImagen(precio,nombre,imagen,gramos,descripcion){
      let compra: TdCompraComponent
      //compra.detallesCompra(precio,nombre,imagen,gramos)
      //this.router.navigate["/compra/precio/nombre/imagen/gramos"]
      this.router.navigateByUrl(`home/compra/${nombre}/${precio}/${imagen}/${gramos}/${descripcion}`)
    }
  

}
